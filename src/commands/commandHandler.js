const { SlashCommandBuilder } = require('discord.js');
const { EmbedBuilder } = require('discord.js');
const config = require('../config.json');
const fs = require('fs');

const commands = {
    ping: {
        builder: new SlashCommandBuilder()
            .setName('ping')
            .setDescription('Zeigt die Reaktionszeit des Bots an'),
        handler: async (interaction) => {
            const sent = await interaction.reply({ content: 'Pinging...' });
            interaction.editReply(`Pong! 🏓 (Latenz: ${sent.createdTimestamp - interaction.createdTimestamp}ms)`);
        },
    },

    // **Erweiterter "setconfig"-Command:**
    setconfig: {
        builder: new SlashCommandBuilder()
            .setName('setconfig')
            .setDescription('Konfiguriert die automatische Rollenzuweisung')
            .addSubcommand(subcommand =>
                subcommand.setName('add')
                    .setDescription('Fügt eine neue Rollenregel hinzu')
                    .addStringOption(option =>
                        option.setName('roleid')
                            .setDescription('Die ID der zuzuweisenden Rolle')
                            .setRequired(true))
                    .addStringOption(option =>
                        option.setName('channelid')
                            .setDescription('Die ID des Kanals, in dem die Rolle zugewiesen wird')
                            .setRequired(true))
                    .addStringOption(option =>
                        option.setName('messageid')
                            .setDescription('Die ID der Nachricht, auf die reagiert werden soll (optional)')),
            )
            .addSubcommand(subcommand =>
                subcommand.setName('list')
                    .setDescription('Zeigt alle konfigurierten Rollenregeln an')
            )
            .addSubcommand(subcommand =>
                subcommand.setName('remove')
                    .setDescription('Entfernt eine existierende Rollenregel')
                    .addStringOption(option =>
                        option.setName('ruleid')
                            .setDescription('Die ID der zu entfernenden Rollenregel (entweder Rollen-ID, Kanal-ID oder Nachrichten-ID)')
                            .setRequired(true))
                    .addBooleanOption(option =>
                        option.setName('allduplicates')
                            .setDescription('Soll alle Regeln mit der gleichen Rollen-ID entfernt werden?')
                            .setRequired(false))
            ),

        handler: async (interaction) => {
            const subcommand = interaction.options.getSubcommand();

            if (subcommand === 'add') {
                const roleId = interaction.options.getString('roleid');
                const channelId = interaction.options.getString('channelid');
                const messageId = interaction.options.getString('messageid');

                if (!interaction.guild.roles.cache.has(roleId)) {
                    return interaction.reply({ content: `Die Rolle mit der ID ${roleId} konnte nicht gefunden werden.`, ephemeral: true });
                }

                if (!interaction.guild.channels.cache.has(channelId)) {
                    return interaction.reply({ content: `Der Kanal mit der ID ${channelId} konnte nicht gefunden werden.`, ephemeral: true });
                }

                const roleRule = {
                    roleId,
                    channelId,
                    messageId,
                    reactions: ['✅', '❌', '❓'],
                };

                config.rolesToReactTo.roleRules.push(roleRule);
                fs.writeFileSync('./config.json', JSON.stringify(config));

                const embed = new EmbedBuilder()
                    .setTitle('Rollenregel hinzugefügt')
                    .setDescription(`Rolle: <@&${roleId}>, Kanal: <#${channelId}>, Nachricht: ${(messageId) ? `<#${messageId}>` : 'Keine'}`);

                return interaction.reply({ embeds: [embed] });
            } else if (subcommand === 'list') {
                const embed = new EmbedBuilder()
                    .setTitle('Konfigurierte Rollenregeln');

                for (const roleRule of config.rolesToReactTo.roleRules) {
                    embed.addFields({
                        name: `Rolle: <@&${roleRule.roleId}>`,
                        value: `Kanal: <#${roleRule.channelId}>, Nachricht: ${(roleRule.messageId) ? `<#${roleRule.messageId}>` : 'Keine'}`
                    });
                }

                return interaction.reply({ embeds: [embed] });
            } else if (subcommand === 'remove') {
                const ruleId = interaction.options.getString('ruleid');
                const allDuplicates = interaction.options.getBoolean('allduplicates');

                if (allDuplicates) {
                    const ruleIndexes = config.rolesToReactTo.roleRules.reduce((indexes, rule, index) => {
                        if (rule.roleId === ruleId) {
                            indexes.push(index);
                        }
                        return indexes;
                    }, []);

                    if (ruleIndexes.length === 0) {
                        return interaction.reply({ content: `Keine Regel mit der ID ${ruleId} gefunden.`, ephemeral: true });
                    }

                    config.rolesToReactTo.roleRules = config.rolesToReactTo.roleRules.filter((_, index) => !ruleIndexes.includes(index));
                    fs.writeFileSync('./config.json', JSON.stringify(config));

                    return interaction.reply({ content: `**${ruleIndexes.length}** Regeln mit der Rollen-ID ${ruleId} wurden entfernt.`, ephemeral: true });
                }

                const ruleIndex = config.rolesToReactTo.roleRules.findIndex(rule => rule.roleId === ruleId || rule.channelId == ruleId || rule.messageId === ruleId);

                if (ruleIndex === -1) {
                    return interaction.reply({ content: `Die Rollenregel mit der ID ${ruleId} konnte nicht gefunden werden.`, ephemeral: true });
                }

                config.rolesToReactTo.roleRules.splice(ruleIndex, 1);
                fs.writeFileSync('./config.json', JSON.stringify(config));

                return interaction.reply({ content: 'Rollenregel erfolgreich entfernt.', ephemeral: true });
            } else {
                return interaction.reply({ content: 'Ungültiger Subcommand.', ephemeral: true });
            }
        },
    },
};

module.exports = { commands };
