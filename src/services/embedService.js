const { EmbedBuilder } = require('discord.js');

const buttons = {
    yesButton: "notificationYesButton",
    noButton: "notificationNoButton",
    blockButton: "notificationBlockButton"
}



module.exports = {
    async getReportEmbed(message, roleID, reactionList, user, reaction) {
        return new Promise((resolve, reject) => {
            const messageContent = message.content.replace(`<\@\&${roleID}>`, "@some-role"); // Removes that @deleted-role from notification messages

            const embed = createTemplateEmbed();
    
            embed.addFields(
                { name: 'Jemand reagierte auf deine Nachricht', value: `<@${user.id}> reagierte mit ${reaction._emoji.name}`, inline: false },
                { name: "Deine Nachricht", value: `\> ${messageContent}` }
            )
    
            Object.keys(reactionList).forEach(currentEmoji => {
                if (reactionList[currentEmoji].length == 0)
                    return;
    
                let usersString = "";
                for (let i = 0; i <= reactionList[currentEmoji].length - 1; i++) {
                    usersString = usersString + `<@${reactionList[currentEmoji][i]}>`;
    
                    // only add ", " if current user isnt the last one 
                    if (!(i == reactionList[currentEmoji].length - 1)) {
                        usersString = usersString + ", ";
                    }
                }
    
                embed.addFields({
                    name: `Reaktionen mit ${currentEmoji}`, value: `${usersString}`
                });
            });
    
            resolve(embed);
        })  
    },

    getNotificationEmbed(timeout) {
        const embed = createTemplateEmbed()
            .setTitle('Benachrichtigung bei Reactions')
            .setDescription('Möchtest du benachrichtigt werden wenn jemand auf deine Nachricht reagiert?')
        return embed
    },

    askForReactionNotification() {
        return createTemplateEmbed()
            .setTitle('Benachrichtigung bei Reactions')
            .setDescription('Bei welchen Reactions möchtest du benachrichtigt werden?')
    }

}

function createTemplateEmbed() {
    return new EmbedBuilder()
        .setColor('#141014')
        .setFooter({ text: "Captain Coalition", iconURL: "https://cdn.discordapp.com/icons/329211845720276992/52734b508929961e0731c00dd2c9f4ae.webp?size=96" });
}