const { Client, GatewayIntentBits, ActivityType, Events } = require('discord.js');
const fs = require("fs")
const { commands } = require('./commands/commandHandler.js');

const config = require("./config");
const reactToRolesMentioned = require('./events/reactToRolesMentioned');

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMessageReactions] })

// Add all events files
const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
    const event = require(`./events/${file}`);
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}



const TOKEN = process.env.BOT_TOKEN;

client.on('ready', async () => {
    console.log(`Logged in as ${client.user.tag}!`);

    client.user.setPresence({
        activities: [{ name: `Brooklyn Nine-Nine`, type: ActivityType.Watching }],
        status: 'online',
    });

    // Register commands
    for (const command of Object.values(commands)) {
        await client.guilds.cache.get(process.env.GUILD_ID).commands.create(command.builder);
        console.log('Successfully registered all (/) commands!');
    }
});

const handlerCache = {}

client.on(Events.InteractionCreate, async interaction => {
    if (interaction.isCommand()) { // Slash-Commands
        const command = commands[interaction.commandName];
        if (!command) return;

        try {
            await command.handler(interaction);
        } catch (error) {
            console.error(error);
            await interaction.reply({ content: 'Ein Fehler beim Ausführen des Commands aufgetreten.', ephemeral: true });
        }
    } else if (interaction.isButton()) { // Button-Interaction
        try {
            // Fetch messages
            const message = await interaction.channel.messages.fetch(interaction.message.reference.messageId);

            const handlerName = interaction.customId.split('-')[0];
            const handler = require(`./handler/${handlerName}Handler`);
            await handler.execute(interaction, message);
        } catch (error) {
            console.error(error);
            await interaction.reply({ content: 'Beim Ausführen der Button-Interaktion ist ein Fehler aufgetreten.', ephemeral: true });
        }
    } else {

    }
});

client.login(TOKEN);
